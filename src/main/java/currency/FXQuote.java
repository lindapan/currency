/* Linda Pan */

package currency;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class FXQuote {
	String currency1;
	String currency2;
	double price;
	HashMap<String, Double> map = new HashMap<>();
	
	
	public FXQuote() {
		this.map = new HashMap<String, Double>();
		
	}
	
	public void readCSV(String csvFile) throws InterruptedException {
		
		BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
            	
                // use comma as separator
                String[] column = line.split(cvsSplitBy);
                
                if (column[0].equals("C1")) continue;
                
//                TimeUnit.SECONDS.sleep(1);
                String key;
                Double value;
                int compare = column[0].compareToIgnoreCase(column[1]);
                
                if (column[2] != null) {
                	if(compare < 0) {
                    	key = column[0] + "," + column[1];
                    	value = Double.parseDouble(column[2]);
                    }else if(compare > 0) {
                    	key = column[1] + "," + column[0];
                    	value = 1/Double.parseDouble(column[2]);
                    }else {
                    	continue;
                    }
                	map.put(key, value);
                }
                
//                System.out.println(map);

//                System.out.println("Country one: " + country[0] + " , Country two=" + country[1] + ", Currency: " + country[2]);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
	
	public double getUSDFXQuote (String country) {
		int compare = country.compareToIgnoreCase("USD");
		if (compare == 0) return 0.0;
		String key;
		Double price;
		
		if (compare < 0) {
			key = country+",USD";
			price = map.get(key);
		} else {
			key = "USD,"+country;
			price = 1/map.get(key);
		}
		return price;
		
	}
	
	public double getFXQuote (String country1, String country2) {
		
		
		String key = "";
		int compare = country1.compareToIgnoreCase(country2);
		if (compare == 0) return 0.0;
		
		if(compare < 0) {
			key = country1 + "," + country2;
		}else if(compare > 0) {
			key = country2 + "," + country1;
		}
		// Direct
		if (map.containsKey(key)) {
			if (compare < 0) {
				return map.get(key);
			}else if (compare > 0) {
				return 1/map.get(key);
			}
		}
		
		// Indirect 
		if(getUSDFXQuote(country1) == 0 || getUSDFXQuote(country2) == 0) {return 0.0;}
		
		return getUSDFXQuote(country1)/getUSDFXQuote(country2);
		
		
	}
	
	public static void main(String[] args) throws InterruptedException {

//        String csvFile = "C:\\workspace\\currency\\rates.csv";
//        
//        FXQuote fxquote = new FXQuote();
//        fxquote.readCSV(csvFile);
////     fxquote.validate("USD", "CAD");
//     
//        System.out.println(fxquote.getFXQuote("CAD", "USD"));
        
    }

}
