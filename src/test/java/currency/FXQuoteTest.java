/*Linda Pan*/

package currency;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

public class FXQuoteTest {
	
	@BeforeClass
	public static void beforeAllTest() throws InterruptedException {
	
	}


	@Test
	public void testEqual() throws InterruptedException {
		String csvFile = "rates.csv";
	      
	    FXQuote fxquote = new FXQuote();
	    fxquote.readCSV(csvFile);
	    assertEquals(0.0,fxquote.getFXQuote("USD", "USD"), 0.001);
	      
	}
	
	@Test
	public void testSamePair() throws InterruptedException {
		String csvFile = "rates.csv";
	      
	    FXQuote fxquote = new FXQuote();
	    fxquote.readCSV(csvFile);
	    assertEquals(fxquote.getFXQuote("USD", "CAD"),1/fxquote.getFXQuote("CAD", "USD"), 0.001);
	      
	}
	
	@Test
	public void testNULL() throws InterruptedException {
		String csvFile = "rates1.csv";
	      
	    FXQuote fxquote = new FXQuote();
	    fxquote.readCSV(csvFile);
	    assertEquals(fxquote.getFXQuote("USD", "CAD"), 0.0, 0.001);
	      
	}
	
	@Test
	public void testIndirect() throws InterruptedException {
		String csvFile = "rates1.csv";
	      
	    FXQuote fxquote = new FXQuote();
	    fxquote.readCSV(csvFile);
	    assertEquals(fxquote.getFXQuote("AED", "AUD"), 0.2723*0.9767, 0.001);
	      
	}
	

}
